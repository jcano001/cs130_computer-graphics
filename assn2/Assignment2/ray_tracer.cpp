/**
 * ray_tracer.cpp
 * CS230 Assignment 2, Winter 2012
 * -------------------------------
 * Implement ray tracer here.
 */

#define SET_RED(P, C)   (P = (((P) & 0x00ffffff) | ((C) << 24)))
#define SET_GREEN(P, C)  (P = (((P) & 0xff00ffff) | ((C) << 16)))
#define SET_BLUE(P, C) (P = (((P) & 0xffff00ff) | ((C) << 8)))

#include "ray_tracer.h"

using namespace std;

const double Object::small_t=1e-6;
//--------------------------------------------------------------------------------
// utility functions
//--------------------------------------------------------------------------------
double sqr(const double x)
{
    return x*x;
}

Pixel Pixel_Color(const Vector_3D<double>& color)
{
    Pixel pixel=0;
    SET_RED(pixel,(unsigned char)(min(color.x,1.0)*255));
    SET_GREEN(pixel,(unsigned char)(min(color.y,1.0)*255));
    SET_BLUE(pixel,(unsigned char)(min(color.z,1.0)*255));
    return pixel;
}
//--------------------------------------------------------------------------------
// Shader
//--------------------------------------------------------------------------------
Vector_3D<double> Phong_Shader::
Shade_Surface(const Ray& ray,const Object& intersection_object,const Vector_3D<double>& intersection_point,const Vector_3D<double>& same_side_normal) const
{
    Vector_3D<double> color;

    // TODO: determine the color
    double diffuse = 0.0;
    double specular = 0.0;
    
    Vector_3D<double> light, half_vector;
    for( int i = 0; i < world.lights.size(); i++ ){
    	light = world.lights[i]->position - intersection_point;
     	light.Normalize();

    	diffuse = max(0.0,Vector_3D<double>::Dot_Product(light,same_side_normal));
    	
    	half_vector = light - ray.direction;
    	half_vector.Normalize();
    	
    	double phong = max(0.0,Vector_3D<double>::Dot_Product(half_vector,same_side_normal));
    	specular = pow(phong,specular_power);
    	
    	Ray shadow;
    	shadow.t_max = ray.t_max;
    	shadow.endpoint = intersection_point + same_side_normal*intersection_object.small_t;
    	shadow.direction = light.Normalized();
    	const Object * obj = world.Closest_Intersection( shadow );
    	if( obj && world.enable_shadows ) continue;
    	
    	color += (color_diffuse*diffuse + color_specular*specular)*(world.lights[i]->Emitted_Light( ray ));
    
    }

    return color;
}

Vector_3D<double> Reflective_Shader::
Shade_Surface(const Ray& ray,const Object& intersection_object,const Vector_3D<double>& intersection_point,const Vector_3D<double>& same_side_normal) const
{
    Vector_3D<double> color;

    // TODO: determine the color
    color = Phong_Shader::Shade_Surface(ray,intersection_object,intersection_point,same_side_normal);
    if(ray.recursion_depth <= world.recursion_depth_limit){
    	Ray reflect;
    	reflect.endpoint = intersection_point+same_side_normal*intersection_object.small_t;
    	reflect.direction = ray.direction-same_side_normal*2*Vector_3D<double>::Dot_Product(ray.direction,same_side_normal);
    	reflect.recursion_depth = ray.recursion_depth+1;
    	color += world.Cast_Ray(reflect,ray)*reflectivity;
    }

    return color;
}

Vector_3D<double> Flat_Shader::
Shade_Surface(const Ray& ray,const Object& intersection_object,const Vector_3D<double>& intersection_point,const Vector_3D<double>& same_side_normal) const
{
    return color;
}

//--------------------------------------------------------------------------------
// Objects
//--------------------------------------------------------------------------------
// determine if the ray intersects with the sphere
// if there is an intersection, set t_max, current_object, and semi_infinite as appropriate and return true
bool Sphere::
Intersection(Ray& ray) const
{
    // TODO    

 		double a, b, c;
  	a = Vector_3D<double>::Dot_Product(ray.direction,ray.direction);
  	b = 2*(Vector_3D<double>::Dot_Product(ray.direction,ray.endpoint-center));
  	c = Vector_3D<double>::Dot_Product(ray.endpoint-center,ray.endpoint-center)-(radius*radius);
    
  	double discriminant=(b*b) -(4*a*c);
  	if( discriminant < 0 ) return false;
  
 	 	 double q;
 	 	if( b < 0 ) q=(-b - sqrt( discriminant ))/2.0;
 	 	else q=(-b + sqrt( discriminant ))/2.0;
  
 	 	double t0=q/a;
 	 	double t1=c/q;

  	if(t0 > t1) 
    {
      double temp = t0;
      t0=t1;
      t1=temp;
    }
  	double t;
  
  	if(t1 < 0) return false;
    
    if( t0 < 0 ) t=t1;
    else t=t0;

    if( ray.semi_infinite == true ){
       ray.semi_infinite = false;
       ray.t_max=t;
       ray.current_object=this;
   	}

    if( t<ray.t_max and t >small_t ){
    	ray.t_max=t;
    	ray.current_object=this;
    	return true;
    }
    else return false;
}

Vector_3D<double> Sphere::
Normal(const Vector_3D<double>& location) const
{
    Vector_3D<double> normal;
    // TODO: set the normal
    normal = (location-center).Normalized();
    return normal;
}

/*std::string Sphere::
GetID() const{
		return "Sphere";
}*/
// determine if the ray intersects with the sphere
// if there is an intersection, set t_max, current_object, and semi_infinite as appropriate and return true
bool Plane::
Intersection(Ray& ray) const
{
    // TODO
		double numerator, denominator;
		
		numerator = Vector_3D<double>::Dot_Product(normal,x1-ray.endpoint);
		denominator = Vector_3D<double>::Dot_Product(normal,ray.direction);
		
		if( denominator == 0 ) return false;
		
	  else{
	  double t = numerator/denominator;
	  	if( ray.semi_infinite && t > small_t ){
	  		ray.semi_infinite = false;
	  		ray.t_max = t;
	  		ray.current_object = this;
	  		return true;
	  	}
	  	else if( t > small_t && t < ray.t_max ){
	  		ray.t_max = t;
	  		ray.current_object = this;
	  		return true;
	  	}
	  }
    return false;
}

Vector_3D<double> Plane::
Normal(const Vector_3D<double>& location) const
{
    return normal;
}

/*std::string Plane::
GetID() const
{
		return "Plane";
}*/
//--------------------------------------------------------------------------------
// Camera
//--------------------------------------------------------------------------------
// Find the world position of the input pixel
Vector_3D<double> Camera::
World_Position(const Vector_2D<int>& pixel_index)
{
    Vector_3D<double> result;
    // TODO 
 		Vector_2D<double> pixel = film.pixel_grid.X( pixel_index );
 		result = focal_point + vertical_vector * pixel.y + horizontal_vector * pixel.x;
 		
    return result;
}
//--------------------------------------------------------------------------------
// Render_World
//--------------------------------------------------------------------------------
// Find the closest object of intersection and return a pointer to it
//   if the ray intersects with an object, then ray.t_max, ray.current_object, and ray.semi_infinite will be set appropriately
//   if there is no intersection do not modify the ray and return 0
const Object* Render_World::
Closest_Intersection(Ray& ray)
{
    // TODO   
    for( int i = 0; i < objects.size(); i++ ){
			objects[i]->Intersection( ray );
    }
    
    const Object* current = ray.current_object;
    
    if( current ) return current;
    return 0;
}

// set up the initial view ray and call 
void Render_World::
Render_Pixel(const Vector_2D<int>& pixel_index)
{
    // TODO
    Ray ray; // TODO: set up the initial view ray here
		ray.endpoint = camera.position;
		ray.direction = camera.World_Position( pixel_index ) - camera.position;

    Ray dummy_root;
    Vector_3D<double> color=Cast_Ray(ray,dummy_root);
    camera.film.Set_Pixel(pixel_index,Pixel_Color(color));
}

// cast ray and return the color of the closest intersected surface point, 
// or the background color if there is no object intersection
Vector_3D<double> Render_World::
Cast_Ray(Ray& ray,const Ray& parent_ray)
{
    // TODO
    Vector_3D<double> color;

    // determine the color here
    const Object* closest = Closest_Intersection( ray );
    if( closest ){
    	color = closest->material_shader->Shade_Surface( ray, *closest, ray.Point(ray.t_max), closest->Normal(ray.Point(ray.t_max)) );
    }
	  //else cin >> pause;
    	//color = background_shader->Shade_Surface( ray, *closest, ray.Point(ray.t_max), closest->Normal(ray.Point(ray.t_max)) );
    

    return color;
}
