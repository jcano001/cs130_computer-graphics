#include <GL/gl.h>
#include <SDL/SDL.h>
#include <iostream>
#include <cmath>
#include <vector>
using namespace std;


// Classes
class vector3
{
public:
	vector3(){}
	vector3(double x, double y) : x(x), y(y), z(0) {}
	vector3(double x, double y, double z) : x(x), y(y), z(z) {}
	vector3 operator+(vector3 v){ return vector3(x+v.x, y+v.y, z+v.z); }
	vector3 operator-(vector3 v){ return vector3(x-v.x, y-v.y, z-v.z); }
	vector3 operator*(double s){ return vector3(s*x, s*y, s*z); }
	bool operator!=(vector3 v){ return (x!=v.x or y!=v.y or z!=v.z); }	
	bool operator==(vector3 v){ return (x==v.x and y==v.y and z==v.z); }
public:
	double x,y,z;
};
//vector3 operator-(vector3 v) {return vector3(0,0,0) - v;}
vector3 operator/(vector3 v, double s) {return v*(1.0/s);}
vector3 operator*(double s, vector3 v) {return v*s;}
double dot(vector3 a, vector3 b) { return (a.x*b.x + a.y*b.y + a.z*b.z); }
vector3 normal(vector3 v) { return (1.0/sqrt(dot(v,v)))*v; }
template<class T> T square(T a) { return a*a; }



//Some defines
const int SCREEN_WIDTH = 400;
const int SCREEN_HEIGHT = 300;

const int XMIN = -SCREEN_WIDTH/2;
const int XMAX = SCREEN_WIDTH/2;
const int YMIN = -SCREEN_HEIGHT/2;
const int YMAX = SCREEN_HEIGHT/2;



//Plots a point
void plot(int x, int y, float r, float g, float b)
{
	glBegin(GL_POINTS);
		glColor3f(r,g,b);
		glVertex2i(x,y);
	glEnd();
}

double quadratic(double a, double b, double c)
{
	double discriminant = (b*b) - (4*a*c);
	if( discriminant < 0 ) return -1;
	else if( discriminant == 0 ) return (-b) / (2 * a);
	double pos_t = ((-b) + sqrt(discriminant)) / (2 * a);
	double neg_t = ((-b) - sqrt(discriminant)) / (2 * a);
	
	if( pos_t >= 0 && neg_t >= 0 ) return pos_t <= neg_t ? pos_t : neg_t;
	else if( pos_t < 0 && neg_t < 0 ) return 0;
	
	return pos_t >= neg_t ? pos_t : neg_t;
}

//Main Function
int main(int nArgs, char** args)
{
	//Initialize the window
	SDL_Init(SDL_INIT_VIDEO);
	SDL_InitSubSystem(SDL_INIT_VIDEO);

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8); //8 bits for red
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8); //8 bits for green
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8); //8 bits for blue
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1); //enable page flipping
	SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32,SDL_OPENGL);
	SDL_WM_SetCaption("CS130 Lab", NULL);

	//Set up the projection - don't worry about this
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(XMIN, XMAX, YMIN, YMAX,-1,1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	vector3 spherePosition(0,0,30);
	double sphereRadius=10;
	vector3 lightPosition(-20,-10,-10);
	vector3 pixelgridCenter(0,0,5);
	vector3 cameraPos(0,0,0);
	vector3 ambientColor(.2f,.2f,.2f);
	vector3 diffuseColor(.5f,.5f,.5f);
	vector3 specularColor(.5f,.5f,.5f);
	vector3 x_incr(.025,0,0);
	vector3 y_incr(0,.025,0);
	

	while(true)
	{
		//Update platform specific stuff
		SDL_Event event;
		SDL_PollEvent(&event); //Get events
		if(event.type == SDL_QUIT) //if you click the [X] exit out
			break;
		//Graphics
		glClear(GL_COLOR_BUFFER_BIT); //Clear the screen
		for(int x = XMIN; x <= XMAX; ++x)
		{
			for(int y = YMIN; y <= YMAX; ++y)
			{
				vector3 d(x*x_incr.x,y*y_incr.y,pixelgridCenter.z);
				vector3 direction = normal(d);
				double a = dot(direction,direction);
				double b = 2*(dot(direction,(cameraPos-spherePosition)));
				double c = dot((cameraPos-spherePosition),(cameraPos-spherePosition)) - (sphereRadius*sphereRadius);

				double discriminant = (b*b) - (4*a*c);

				double t = quadratic(a,b,c);


				vector3 color(0,0,0); // fill this in with the appropriate colors
				if( t < 0 ) color = vector3(0,0,0);
				else{
					color = ambientColor;
					vector3 lightNorm = normal(lightPosition);
					vector3 norm = normal((t*direction)-spherePosition);
					//color = color + norm+(ambientColor + diffuseColor + specularColor);
				}
	
				    
		    plot(x,y,color.x,color.y,color.z);
		    // to help with the math:
		    // http://www.csee.umbc.edu/~olano/435f02/ray-sphere.html	
			}
		}
		SDL_GL_SwapBuffers(); //Finished drawing
	}



	//Exit out
	SDL_Quit();
	return 0;
}
