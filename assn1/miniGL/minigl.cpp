/**
 * minigl.cpp
 * CS248 Assignment 1, Winter 2010
 * -------------------------------
 * Implement miniGL here.
 * Do not use any additional files
 */

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <cmath>
#include "minigl.h"

#include <vector>
#include <stack>
using namespace std;


/**
 * Classes
 */
class vector4
{
public:
	vector4() {}
	vector4(MGLfloat x, MGLfloat y) : x(x), y(y), z(0), w(1) {}
	vector4(MGLfloat x, MGLfloat y, MGLfloat z) : x(x), y(y), z(z), w(1) {}
	vector4(MGLfloat x, MGLfloat y, MGLfloat z, MGLfloat w) : x(x), y(y), z(z), w(w) {}
	vector4 operator+(vector4 v){ return vector4(x+v.x, y+v.y, z+v.z, w+v.w); }
	vector4 operator-(vector4 v){ return vector4(x-v.x, y-v.y, z-v.z, w-v.w); }
	vector4 operator*(MGLfloat s){ return vector4(s*x, s*y, s*z, s*w); }
	bool operator!=(vector4 v){ return (x!=v.x or y!=v.y or z!=v.z or w!=v.w); }	
	bool operator==(vector4 v){ return (x==v.x and y==v.y and z==v.z and w==v.w); }

public:
	MGLfloat x,y,z,w;
};

class matrix4x4
{
	//implementation here
	public:
        matrix4x4() {
                data[0] = 1; data[1] = 0; data[2] = 0; data[3] = 0;
                data[4] = 0; data[5] = 1; data[6] = 0; data[7] = 0;
                data[8] = 0; data[9] = 0; data[10] = 1; data[11] = 0;
                data[12] = 0; data[13] = 0; data[14] = 0; data[15] = 1;
        }
        /* matrix[16] is passed in as
				 *   ( a0  a4  a8  a12 )
				 *   ( a1  a5  a9  a13 )
 				 *   ( a2  a6  a10 a14 )
				 *   ( a3  a7  a11 a15 )
				 *
				 *return
				 *   (  a0  a1  a2  a3  )
				 *   (  a4  a5  a6  a7  )
 				 *   (  a8  a9  a10 a11 )
				 *   (  a12 a13 a14 a15 )
         */
     		matrix4x4(const MGLfloat matrix[16]) {
     			      data[0] = matrix[0]; data[1] = matrix[4]; data[2] = matrix[8]; data[3] = matrix[12];
                data[4] = matrix[1]; data[5] = matrix[5]; data[6] = matrix[9]; data[7] = matrix[13];
                data[8] = matrix[2]; data[9] = matrix[6]; data[10] = matrix[10]; data[11] = matrix[14];
                data[12] = matrix[3]; data[13] = matrix[7]; data[14] = matrix[11]; data[15] = matrix[15];
        }

        /* lhs is passed in as		rhs is passed in as				
				 * (  a0  a1  a2  a3  )	    (  d0  d1  d2  d3  )
				 * (  a4  a5  a6  a7  )  x  (  d4  d5  d3  d7  )
 				 * (  a8  a9  a10 a11 )     (  d8  d9  d10 d11 )
				 * (  a12 a13 a14 a15 )     (  d12 d13 d14 d15 )
         */
				matrix4x4 operator*(matrix4x4 m){
							MGLfloat matrix[16] = {
								//Row 1
								//d0
								data[0]*m.data[0] + data[1]*m.data[4] + data[2]*m.data[8] + data[3]*m.data[12],
								//d4
								data[4]*m.data[0] + data[5]*m.data[4] + data[6]*m.data[8] + data[7]*m.data[12],
								//d8
								data[8]*m.data[0] + data[9]*m.data[4] + data[10]*m.data[8] + data[11]*m.data[12],
								//d12
								data[12]*m.data[0] + data[13]*m.data[4] + data[14]*m.data[8] + data[15]*m.data[12],
								
								//Row2
								//d1
								data[0]*m.data[1] + data[1]*m.data[5] + data[2]*m.data[9] + data[3]*m.data[13],
								//d5
								data[4]*m.data[1] + data[5]*m.data[5] + data[6]*m.data[9] + data[7]*m.data[13],
								//d9
								data[8]*m.data[1] + data[9]*m.data[5] + data[10]*m.data[9] + data[11]*m.data[13],
								//d13
								data[12]*m.data[1] + data[13]*m.data[5] + data[14]*m.data[9] + data[15]*m.data[13],
								
								//Row 3
								//d2
								data[0]*m.data[2] + data[1]*m.data[6] + data[2]*m.data[10] + data[3]*m.data[14],
								//d6
								data[4]*m.data[2] + data[5]*m.data[6] + data[6]*m.data[10] + data[7]*m.data[14],
								//d10
								data[8]*m.data[2] + data[9]*m.data[6] + data[10]*m.data[10] + data[11]*m.data[14],
								//d14
								data[12]*m.data[2] + data[13]*m.data[6] + data[14]*m.data[10] + data[15]*m.data[14],
				
								//Row 4
								//d3
								data[0]*m.data[3] + data[1]*m.data[7] + data[2]*m.data[11] + data[3]*m.data[15],
								//d7
								data[4]*m.data[3] + data[5]*m.data[7] + data[6]*m.data[11] + data[7]*m.data[15],
								//d11
								data[8]*m.data[3] + data[9]*m.data[7] + data[10]*m.data[11] + data[14]*m.data[15],
								//d15
								data[12]*m.data[3] + data[13]*m.data[7] + data[14]*m.data[11] + data[15]*m.data[15],
							};
							return matrix4x4(matrix);
				}
        inline MGLfloat operator[](int index) const { return data[index]; }     

	public:
        MGLfloat data[16];
};

vector4 operator*(const vector4& lhs, const matrix4x4& rhs){
				MGLfloat x,y,z,w;
				x = lhs.x*rhs[0] + lhs.y*rhs[1] + lhs.z*rhs[2] + lhs.w*rhs[3];
				y = lhs.x*rhs[4] + lhs.y*rhs[5] + lhs.z*rhs[6] + lhs.w*rhs[7];
				z = lhs.x*rhs[8] + lhs.y*rhs[9] + lhs.z*rhs[10] + lhs.w*rhs[11];
				w = lhs.x*rhs[12] + lhs.y*rhs[13] + lhs.z*rhs[14] + lhs.w*rhs[15];
				return vector4(x,y,z,w);
}

vector4 normalize(const vector4& v){
				MGLfloat length = sqrt(v.x*v.x + v.y*v.y + v.z*v.z + v.w*v.w);
				if( length!= 0 ) return vector4(v.x/length, v.y/length, v.z/length, v.w/length);
				else return vector4();
}

/**
 * Global variables
 */
stack<matrix4x4> modelview;
stack<matrix4x4> projection;
stack<matrix4x4>* stack_pointer = &modelview;

MGLbool building = false;
MGLpoly_mode poly_mode;

MGLbyte RED,GREEN,BLUE;
vector<MGLbyte> red_val;
vector<MGLbyte> green_val;
vector<MGLbyte> blue_val;

MGLfloat depth;

vector<vector4> vertexList; //holds vertices for the mglVertex3 calls
int vertexIndex = 0;

MGLpixel framebuffer[MGL_SCREEN_WIDTH][MGL_SCREEN_HEIGHT];
MGLfloat depthbuffer[MGL_SCREEN_WIDTH][MGL_SCREEN_HEIGHT];

/**
 * This is a helper function. It plots a white pixel on the frame buffer.
 */
void plot(unsigned int x, unsigned int y)
{
	//you may need to modify this function to suit your needs
	if(x >= MGL_SCREEN_WIDTH or y >= MGL_SCREEN_HEIGHT)
		return;

	MGLpixel color = 0;
	MGL_SET_RED(color, RED);
	MGL_SET_GREEN(color, GREEN);
	MGL_SET_BLUE(color, BLUE);
	
	if(framebuffer[x][y] == 0){
		framebuffer[x][y] = color;
		depthbuffer[x][y] = depth;
	}
	else if(depth < depthbuffer[x][y]){
		framebuffer[x][y] = color;
	}
}

/**
 * Initializes the miniGL system.
 */
void mglInit()
{
	//Setup the matrix stacks
	matrix4x4 identity;
	//you need to set the variable 'identity' to the identity matrix right here.
	identity = matrix4x4();

	modelview.push(identity);
	projection.push(identity);


	//clear the frame buffer and the depth buffer
	MGLpixel color = 0;
	for(unsigned x = 0; x < MGL_SCREEN_WIDTH; x++)
	{
		for(unsigned y = 0; y < MGL_SCREEN_HEIGHT; ++y)
		{
			framebuffer[x][y] = color;
			depthbuffer[x][y] = 1.0f;
		}
	}
}

/**
 * Standard macro to report errors
 */
inline void MGL_ERROR(const char* description) {
    printf("%s\n", description);
    exit(1);
}


/**
 * Helper draw_line Function
 */
void draw_line(int x0, int y0, int x1, int y1)
{
    float dx = abs(x1 - x0);
    float dy = abs(y1 - y0);
    
    int sx, sy;
    if( x0 < x1 ) sx = 1;
    else sx = -1;
    if( y0 < y1 ) sy = 1;
    else sy = -1;
    float err = dx - dy;

    float m = dy/dx;
    float b = y0 - m*x0;

		while(1){
			plot(x0,y0);
			if( x0 == x1 && y0 == y1 ) break;
			float e2 = 2*err;
			if( e2 > -dy ){
				err = err - dy;
				x0 = x0+sx;
			}
			if ( e2 < dx ){
				err = err + dx;
				y0 = y0 + sy;
			}
		}
		
    return;
}


/**
 * Read pixel data starting with the pixel at coordinates
 * (0, 0), up to (width,  height), into the array
 * pointed to by data.  The boundaries are lower-inclusive,
 * that is, a call with width = height = 1 would just read
 * the pixel at (0, 0).
 */
void mglReadPixels(MGLsize width,
                   MGLsize height,
                   MGLpixel *data)
{
	for(unsigned x = 0; x < width; x++)
		for(unsigned y = 0; y < height; ++y)
			data[y*width+x] = framebuffer[x][y];
}

/**
 * Start specifying the vertices for a group of primitives,
 * whose type is specified by the given mode.
 */
void mglBegin(MGLpoly_mode mode)
{
	if( mode == MGL_TRIANGLES || mode == MGL_QUADS ){
		poly_mode = mode;
		vertexIndex = 0;
		building = true;
	}
	else MGL_ERROR("Invalid polygon mode. Param must be MGL_TRIANGLES or MGL_QUADS");
}

/**
 * Stop specifying the vertices for a group of primitives.
 */
void mglEnd()
{
	if( building == false ) MGL_ERROR("Expected call to mglBegin()");
	building = false;
}

/**
 * Specify a two-dimensional vertex; the x- and y-coordinates
 * are explicitly specified, while the z-coordinate is assumed
 * to be zero.  Must appear between calls to mglBegin() and
 * mglEnd().
 */
void mglVertex2(MGLfloat x,
                MGLfloat y)
{
	mglVertex3(x,y,0);
}

/**
 * Specify a three-dimensional vertex.  Must appear between
 * calls to mglBegin() and mglEnd().
 */
void mglVertex3(MGLfloat x,
                MGLfloat y,
                MGLfloat z)
{
	if( building ){
		vector4 v(x,y,z);

		matrix4x4 curr = stack_pointer->top();
		
		vector4 model_view = (v*modelview.top());

		vector4 NDC = (model_view*projection.top());
		
		if( NDC.w != 0 ){
			NDC.x = NDC.x/NDC.w;
			NDC.y = NDC.y/NDC.w;
			NDC.z = NDC.z/NDC.w;
			NDC.w = NDC.w/NDC.w;
		}

		// BEGIN Calculate Viewport Transformation Matrix *********************
		vector4 modelview_projection = NDC;
		matrix4x4 viewport = matrix4x4();
	
		mglLoadIdentity();
		mglTranslate(1,1,1);
		viewport = viewport*stack_pointer->top();	
		
		mglLoadIdentity();
		mglScale(MGL_SCREEN_WIDTH/2,MGL_SCREEN_HEIGHT/2,0);
		viewport = stack_pointer->top()*viewport;

		mglLoadIdentity();
		mglTranslate(-0.5,-0.5,0);
		viewport = stack_pointer->top()*viewport;

		modelview_projection = modelview_projection*viewport;
		// END Calculate Viewport Transformation Matrix *********************
		
		stack_pointer->top() = curr;

		depth = modelview_projection.z;
		vertexList.push_back(modelview_projection);
	}
	
	else MGL_ERROR("Expected mglBegin() before mglVertex3(MGLfloat x, MGLfloat y, MGLfloat z, MGLfloat w)");
	
	RED = red_val[red_val.size()-1];
	GREEN = green_val[green_val.size()-1];
	BLUE = blue_val[blue_val.size()-1];
	
	if( poly_mode == MGL_TRIANGLES && vertexList.size() % 3 == 0 ){	
		for(int i = 0; i < vertexList.size(); i += 3){		
			vector4 a = vertexList[i];
			vector4 b = vertexList[i+1];
			vector4 c = vertexList[i+2];
			draw_line(a.x,a.y,b.x,b.y);
			draw_line(b.x,b.y,c.x,c.y);
			draw_line(c.x,c.y,a.x,a.y);
		}
			
		red_val.pop_back();
		green_val.pop_back();
		blue_val.pop_back();
		vertexList.clear();
	}
	else if( poly_mode == MGL_QUADS && vertexList.size() % 4 == 0){
		for(int i = 0; i < vertexList.size(); i += 4){	
			vector4 a = vertexList[i];
			vector4 b = vertexList[i+1];
			vector4 c = vertexList[i+2];
			vector4 d = vertexList[i+3];
			draw_line(a.x,a.y,b.x,b.y);
			draw_line(b.x,b.y,c.x,c.y);
			draw_line(c.x,c.y,d.x,d.y);
			draw_line(d.x,d.y,a.x,a.y);
		}
			
		red_val.pop_back();
		green_val.pop_back();
		blue_val.pop_back();
		vertexList.clear();
	}
}

/**
 * Set the current matrix mode (modelview or projection).
 */
void mglMatrixMode(MGLmatrix_mode mode)
{
	if(mode == MGL_MODELVIEW){
		//current_matrix_pointer = &modelview_matrix;
		stack_pointer = &modelview;
	}
	else if(mode == MGL_PROJECTION){
		//current_matrix_pointer = &projection_matrix;
		stack_pointer = &projection;
	}
}

/**
 * Push a copy of the current matrix onto the stack for the
 * current matrix mode.
 */
void mglPushMatrix()
{
	stack_pointer->push(stack_pointer->top());
	
}

/**
 * Pop the top matrix from the stack for the current matrix
 * mode.
 */
void mglPopMatrix()
{
	if( stack_pointer->empty() ) MGL_ERROR("Cannot pop matrix from empty stack");
	//*current_matrix_pointer = stack_pointer->top();
	stack_pointer->pop();
}

/**
 * Replace the current matrix with the identity.
 */
void mglLoadIdentity()
{
	//*current_matrix_pointer = matrix4x4();
	stack_pointer->top() = matrix4x4();
}

/**
 * Replace the current matrix with an arbitrary 4x4 matrix,
 * specified in column-major order.  That is, the matrix
 * is stored as:
 *
 *   ( a0  a4  a8  a12 )
 *   ( a1  a5  a9  a13 )
 *   ( a2  a6  a10 a14 )
 *   ( a3  a7  a11 a15 )
 *
 * where ai is the i'th entry of the array.
 */
void mglLoadMatrix(const MGLfloat *matrix)
{
	//*current_matrix_pointer = matrix4x4(matrix);
	stack_pointer->top() = matrix4x4(matrix);
}

/**
 * Multiply the current matrix by an arbitrary 4x4 matrix,
 * specified in column-major order.  That is, the matrix
 * is stored as:
 *
 *   ( a0  a4  a8  a12 )
 *   ( a1  a5  a9  a13 )
 *   ( a2  a6  a10 a14 )
 *   ( a3  a7  a11 a15 )
 *
 * where ai is the i'th entry of the array.
 */
void mglMultMatrix(const MGLfloat *matrix)
{
	stack_pointer->top() = (stack_pointer->top()) * (matrix4x4(matrix));
	//*current_matrix_pointer = mult;
}

/**
 * Multiply the current matrix by the translation matrix
 * for the translation vector given by (x, y, z).
 */
void mglTranslate(MGLfloat x,
                  MGLfloat y,
                  MGLfloat z)
{
	MGLfloat matrix[16] = {1,0,0,0, 0,1,0,0, 0,0,1,0, x,y,z,1};
	mglMultMatrix(matrix);
}

/**
 * Multiply the current matrix by the rotation matrix
 * for a rotation of (angle) degrees about the vector
 * from the origin to the point (x, y, z).
 */
void mglRotate(MGLfloat angle,
               MGLfloat x,
               MGLfloat y,
               MGLfloat z)
{
	vector4 v = normalize(vector4(x,y,z));
	MGLfloat c = cos(angle);
	MGLfloat s = sin(angle);
	
	MGLfloat vxx = v.x*v.x;
	MGLfloat vyy = v.y*v.y;
	MGLfloat vzz = v.z*v.z;
	MGLfloat vxy = v.x*v.y;
	MGLfloat vxz = v.x*v.z;
	MGLfloat vyz = v.y*v.z;
	
	MGLfloat matrix[16] = {
				vxx+(1-vxx)*c, vxy*(1-c)+vzz*s, vxz*(1-c)-vyy*s,0,
				vxy*(1-c)-vzz*s, vyy+(1-vyy)*c, vyz*(1-c)+vxx*s,0,
				vxz*(1-c)+vyy*s,vyz*(1-c)-vxx*s, vzz+(1-vzz)*c,0,
				0, 0, 0, 1
	};
	mglMultMatrix(matrix);
}

/**
 * Multiply the current matrix by the scale matrix
 * for the given scale factors.
 */
void mglScale(MGLfloat x,
              MGLfloat y,
              MGLfloat z)
{
	MGLfloat matrix[16] = {x,0,0,0, 0,y,0,0, 0,0,z,0, 0,0,0,1};
	mglMultMatrix(matrix);
}

/**
 * Multiply the current matrix by the perspective matrix
 * with the given clipping plane coordinates.
 */
void mglFrustum(MGLfloat left,
                MGLfloat right,
                MGLfloat bottom,
                MGLfloat top,
                MGLfloat near,
                MGLfloat far)
{
	MGLfloat A = (right+left)/(right-left);
	MGLfloat B = (top+bottom)/(top-bottom);
	MGLfloat C = -1*((far+near)/(far-near));
	MGLfloat D = -1*((2*far*near)/(far-near));
	MGLfloat matrix[16] = {
					((2*near)/(right-left)),0,0,0, 
					0,((2*near)/(top-bottom)),0,0, 
					A, B, C, -1,
					0,0,D,0
					};
	mglMultMatrix(matrix);
}

/**
 * Multiply the current matrix by the orthographic matrix
 * with the given clipping plane coordinates.
 */
void mglOrtho(MGLfloat left,
              MGLfloat right,
              MGLfloat bottom,
              MGLfloat top,
              MGLfloat near,
              MGLfloat far)
{
	MGLfloat matrix[16] = {
          (2/(right-left)),0,0,0,
          0,(2/(top-bottom)),0,0,
          0,0,-(2/(far-near)),0,
          -1*((right+left)/(right-left)), -1*((top+bottom)/(top-bottom)), -1*((far+near)/(far-near)), 1
        	};

	mglMultMatrix(matrix);
}

/**
 * Set the current color for drawn shapes.
 */
void mglColor(MGLbyte red,
              MGLbyte green,
              MGLbyte blue)
{
	red_val.push_back(red);
	green_val.push_back(green);
	blue_val.push_back(blue);
}

