# CS 130 - Computer Graphics
## UCR | Winter 2013
> A study of the fundamentals of computer graphics necessary to design and build graphics applications. Examines raster graphics algorithms including scan-converting graphics primitives, anti-aliasing, and clipping. Also covers geometric transformations, viewing, solid modeling techniques, hidden-surface removal algorithms, color models, illumination, and shading. 

## assn1
> `miniGL` is a subset of the OpenGL library, writtin in C++

## assn2
> `ray_tracer` is a program demonstrating understanding of ray tracing through a viewport