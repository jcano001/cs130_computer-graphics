uniform sampler2D tex;
varying vec3 N;
float NdotHV;
vec3 specular;

void main()
{
	vec3 L = normalize(gl_LightSource[0].position);
	vec3 normal = normalize(N);

	
	float intensity = dot(L, normal);
	vec4 texture = texture2D(tex, gl_TexCoord[0].xy);
	
	if (intensity > 0.0) {

		// normalize the half-vector, and then compute the
		// cosine (dot product) with the normal
		NdotHV = max(dot(normal, gl_LightSource[0].halfVector.xyz),0.0);
		specular = gl_FrontMaterial.specular * gl_LightSource[0].specular *
				pow(NdotHV,gl_FrontMaterial.shininess);
	}

	gl_FragColor.xyz = texture*(intensity*gl_FrontMaterial.diffuse.rgb + gl_FrontMaterial.ambient.rgb + specular);
	
	gl_FragColor.a = 1.0;
}
