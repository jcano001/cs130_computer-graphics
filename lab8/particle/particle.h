#ifndef PARTICLE_H
#define PARTICLE_H

#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "vector.h"


struct particle
{
	public:
		particle()
			:acceleration(vector3()),pos(vector3()),velocity(vector3()),pos2(vector3())
		{
			color[0] = 0.9;
			color[1] = 0.5;
			color[2] = 0;
		}
		
		particle(vector3 acceleration, vector3 pos, vector3 velocity)
			:acceleration(acceleration), pos(pos), velocity(velocity), pos2(pos)
		{
			color[0] = 0.9;
			color[1] = 0.5;
			color[2] = 0;
		}
		
		
		


	//protected:
		vector3 acceleration,pos,velocity,pos2;
		float color[3];
};

#endif
